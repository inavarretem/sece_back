var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
const { poolPromise,sql } = require('./db')
var jwt = require('jsonwebtoken');

var decodedToken = '';
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* BEGIN SECURITY */

router.get('/getUsers', verifyToken,async (req, res) => {
  console.log(decodedToken.site);
  try {
    const pool = await poolPromise
    const request = await pool.request()
    request.input('site', sql.NVarChar(4), decodedToken.site);
    request.query("SELECT U.User_intUserNo,U.User_strFirstName, U.User_strLastName,U.User_strPassword, UG.UserG_strDesc FROM tblUser U JOIN tblUser_Group UG ON U.UserG_strCode = UG.UserG_strCode WHERE U.Site_strCode = @site", (err,result) => {
      if (err) {
        throw err
      }
      res.json(result.recordset).status(200);
    });
  } catch (err) {
    console.log(err);
    res.send(err.message).status(500);
  }
});

router.get('/getUserGroup', verifyToken,async (req, res) => {
  try {
    const pool = await poolPromise
    const result = await pool.request().query("SELECT UserG_strCode,UserG_strDesc FROM tblUser_Group ORDER BY UserG_intLevel");
    res.json(result.recordset).status(200);
  } catch (err) {
    console.log(err);
    res.send(err.message).status(500);
  }
});
router.get('/getAppointments',async (req, res) => {
  console.log(decodedToken.site);
  try {
    const pool = await poolPromise
    const request = await pool.request()
    request.input('site', sql.NVarChar(4), decodedToken.site);
    request.query("SELECT Subject,Location,StartTime,EndTime,IsAllDay,StartTimezone,EndTimezone,Description,RecurrenceRule,Id FROM tblAppointment WHERE Site_strCode = @site", (err,result) => {
      if (err) {
        throw err
      }
      console.log(result.recordset);
      res.json(result.recordset).status(200);
    });
  } catch (err) {
    console.log(err);
    res.send(err.message).status(500);
  }
});
router.post('/getAppointments/([\$])batch',async (req, res) => {
  console.log(decodedToken.site);
  try {
    const pool = await poolPromise
    const request = await pool.request()
    request.input('site', sql.NVarChar(4), decodedToken.site);
    request.query("SELECT Subject,Location,StartTime,EndTime,IsAllDay,StartTimezone,EndTimezone,Description,RecurrenceRule,Id FROM tblAppointment WHERE Site_strCode = @site", (err,result) => {
      if (err) {
        throw err
      }
      console.log(result.recordset);
      res.json(result.recordset).status(200);
    });
  } catch (err) {
    console.log(err);
    res.send(err.message).status(500);
  }
});
router.post('/addAppoinment', verifyToken,async (req,res) => {
  /*

  dataSource: [ {
    Subject: 'Prueba',
    Location: 'Dallas',
    StartTime: '2020-01-06T15:00:00.000Z',
    EndTime: '2020-01-06T15:30:00.000Z',
    IsAllDay: false,
    StartTimezone: null,
    EndTimezone: null,
    Description: '1029312',
    RecurrenceRule: null,
    Id: 3 } ]

    */
  console.log(req.body[0]);
  var sw = 0;
  switch (req.body[0].IsAllDay) {
    case false:
        sw = 0;
      break;
    case true:
        sw = 1;
      break;
    default:

  }
  const pool = await poolPromise;
  const transaction = new sql.Transaction(pool);
  transaction.begin(err => {
    // ... error checks
    err ? res.send(err).status(500) : console.log("No error");
    const request = new sql.Request(transaction)
    request.input('Subject', sql.NVarChar(1000),req.body[0].Subject);
    request.input('Location', sql.NVarChar(1000), req.body[0].Location);
    request.input('StartTime', sql.DateTime, req.body[0].StartTime);
    request.input('EndTime', sql.DateTime, req.body[0].EndTime);
    request.input('IsAllDay', sql.Int, sw);
    request.input('StartTimezone', sql.DateTime, req.body[0].StartTimezone);
    request.input('EndTimezone', sql.DateTime, req.body[0].EndTimezone);
    request.input('Description', sql.NVarChar(1000), req.body[0].Description);
    request.input('RecurrenceRule', sql.NVarChar(1000), req.body[0].RecurrenceRule);
    request.input('Id', sql.Int, req.body[0].Id);
    request.input('site', sql.NVarChar(1000), decodedToken.site);
    request.query("INSERT INTO tblAppointment (Subject,Location,StartTime,EndTime,IsAllDay,StartTimezone,EndTimezone,Description,RecurrenceRule,Id,Site_strCode) VALUES (@Subject,@Location,@StartTime,@EndTime,@IsAllDay,@StartTimezone,@EndTimezone,@Description,@RecurrenceRule,@Id,@site)", (err, result) => {
      // ... error checks
      if (err) {
        console.log(err);
        res.send(err).status(500)
      } else {
        console.log(result);
        res.send(result.rowsAffected).status(200);
      }
      transaction.commit(err => {
        // ... error checks
        err ? console.log("Problema con Commit") : console.log("Commit correcto");
      })
    })
  })
});
router.post('/addUser', verifyToken,async (req,res) => {
  userGroup = '';
  console.log(decodedToken);
  switch (req.body.UserG_strDesc) {
    case 'Directivo':
    userGroup = 'DIR'
    break;
    case 'Médico':
    userGroup = 'MED'
    break;
    case 'SysAdmin':
    userGroup = 'SYS'
    break;
    case 'Administrativo':
    userGroup = 'ADM'
    break;
    case 'Derechohabiente':
    userGroup = 'HAB'
    break;
    default:userGroup = 'HAB'

  }
  bcrypt.hash(req.body.User_strPassword, 10, async function(err, hash) {
    const pool = await poolPromise;
    const transaction = new sql.Transaction(pool);
    transaction.begin(err => {
      // ... error checks
      err ? res.send(err).status(500) : console.log("No error");
      const request = new sql.Request(transaction)
      request.input('User_intUserNo', sql.Int, req.body.User_intUserNo);
      request.input('User_strFirstName', sql.NVarChar(20), req.body.User_strFirstName);
      request.input('site', sql.NVarChar(4), decodedToken.site);
      request.input('User_strLastName', sql.NVarChar(30), req.body.User_strLastName);
      request.input('UserG_strCode', sql.VarChar(10), userGroup);
      request.input('User_strPassword', sql.NVarChar(1000), hash);
      request.query("INSERT INTO tblUser (User_intUserNo,User_strFirstName,User_strLastName,UserG_strCode,User_strPassword,User_strEnabled,Site_strCode) VALUES (@User_intUserNo,RTRIM(@User_strFirstName),@User_strLastName,@UserG_strCode,@User_strPassword,'Y',@site)", (err, result) => {
        // ... error checks
        err ? res.send(err).status(500) : res.send(result.rowsAffected).status(200);;
        transaction.commit(err => {
          // ... error checks
          err ? console.log("Problema con Commit") : console.log("Commit correcto");
        })
      })
    })
  });
});

router.post('/login', async (req, res) => {
  console.log(req.body);
  const pool = await poolPromise
  const request = await pool.request();
  request.input('User_intUserNo', sql.Int, req.body.id);
  request.input('site', sql.Int, req.body.site);
  request.query("SELECT u.User_intUserNo,u.User_strFirstName,u.User_strLastName,u.User_strPassword,(SELECT Site_strCode FROM tblSite WHERE Site_strCode = @site) [Site],(SELECT Site_strName FROM tblSite WHERE Site_strCode = @site) [SiteName], (SELECT vcModuleRights FROM tblSecRoleRights  WHERE vcRoleName in ( SELECT UserG_strCode FROM tblUser WHERE User_intUserNo = @User_intUserNo AND Site_strCode = @site) AND vcModuleName = 'Clinic') AS 'Clinic', (SELECT vcModuleRights FROM tblSecRoleRights  WHERE vcRoleName in ( SELECT UserG_strCode FROM tblUser WHERE User_intUserNo = @User_intUserNo AND Site_strCode = @site) AND vcModuleName = 'Access') AS 'Access', (SELECT vcModuleRights FROM tblSecRoleRights  WHERE vcRoleName in ( SELECT UserG_strCode FROM tblUser WHERE User_intUserNo = @User_intUserNo AND Site_strCode = @site) AND vcModuleName = 'Admin') AS 'Admin' FROM tblUser u WHERE User_intUserNo = @User_intUserNo AND Site_strCode = @site", (err,result) =>{
    if (err) {
      console.log(err);
    }
    console.log(result.recordset)
    if (result.recordset === undefined || result.recordset.length == 0 || (result.recordset[0].SiteName === undefined || !result.recordset[0].SiteName )) {
      res.json({message:'Combinación incorrecta '}).status(400);
    }
    else {
      bcrypt.compare(req.body.password, result.recordset[0].User_strPassword, function(err, response) {
        if (response) {
          var token = jwt.sign({ username: result.recordset[0].User_strFirstName, id:result.recordset[0].User_intUserNo,accessClinic:result.recordset[0].Clinic,accessAccess:result.recordset[0].Access,accessAdmin:result.recordset[0].Admin,site:result.recordset[0].Site,sitename:result.recordset[0].SiteName }, 'secret',{expiresIn: '1h'});
          console.log(token);
          return res.status(200).json({access:token});
        }
        else {
          res.json({message: 'Contraseña erronea'}).status(400);
        }
      });
    }
  });
});

router.post('/updateUser', verifyToken,async (req, res) => {
  userGroup = '';
  switch (req.body.UserG_strDesc) {
    case 'Directivo':
    userGroup = 'DIR'
    break;
    case 'Médico':
    userGroup = 'MED'
    break;
    case 'SysAdmin':
    userGroup = 'SYS'
    break;
    case 'Administrativo':
    userGroup = 'ADM'
    break;
    case 'Derechohabiente':
    userGroup = 'HAB'
    break;
    default:userGroup = 'HAB'

  }
  const pool = await poolPromise
  const request = await pool.request();
  request.input('User_intUserNo', sql.Int, req.body.User_intUserNo);
  request.input('UserG_strDesc', sql.VarChar(50), userGroup);
  request.query("UPDATE tblUser SET UserG_strCode = @UserG_strDesc WHERE User_intUserNo = @User_intUserNo").then(result => {
    res.send(result.rowsAffected);
  });

});


router.get('/checkAdminPermission', verifyToken, async (req, res, next) => {
  const pool = await poolPromise
  const request = await pool.request();
  request.input('User_intUserNo', sql.Int, decodedToken.id);
  request.query("SELECT vcModuleRights FROM tblSecRoleRights  WHERE vcRoleName in ( SELECT UserG_strCode FROM tblUser WHERE User_intUserNo = @User_intUserNo) AND vcModuleName = 'Admin'", (err,result) =>{
    if (result.recordset[0].vcModuleRights == 'Y') {
      res.json({ access: true});
    }
    else {
      res.json({ access: false});
    }
  });
});

router.get('/checkAccessPermission', verifyToken, async (req, res, next) => {
  const pool = await poolPromise
  const request = await pool.request();
  request.input('User_intUserNo', sql.Int, decodedToken.id);
  request.query("SELECT vcModuleRights FROM tblSecRoleRights  WHERE vcRoleName in ( SELECT UserG_strCode FROM tblUser WHERE User_intUserNo = @User_intUserNo) AND vcModuleName = 'Access'", (err,result) =>{
    if (result.recordset[0].vcModuleRights == 'Y') {
      res.json({ access: true});
    }
    else {
      res.json({ access: false});
    }
  });
});
router.get('/checkClinicPermission', verifyToken, async (req, res, next) => {
  const pool = await poolPromise
  const request = await pool.request();
  request.input('User_intUserNo', sql.Int, decodedToken.id);
  request.query("SELECT vcModuleRights FROM tblSecRoleRights  WHERE vcRoleName in ( SELECT UserG_strCode FROM tblUser WHERE User_intUserNo = @User_intUserNo) AND vcModuleName = 'Clinic'", (err,result) =>{
    if (result.recordset[0].vcModuleRights == 'Y') {
      res.json({ access: true});
    }
    else {
      res.json({ access: false});
    }
  });
});
router.get('/username', verifyToken, async (req, res, next) => {
  return res.status(200).json(decodedToken);
});

function verifyToken(req, res, next){
  let token = req.query.token;
  jwt.verify(token,'secret', function(err,tokenData) {
    if (err) {
      return res.status(400).json({message:'unauthorized request'})
    }
    if (tokenData) {
      decodedToken = tokenData;
      next();
    }
  })
}
/* SECURITY */

/* ADMINISTRATIVOS */

router.post('/addPatient', verifyToken,async (req,res) => {
  console.log(req.body);
  const pool = await poolPromise;
  const transaction = new sql.Transaction(pool);
  transaction.begin(err => {
    // ... error checks
    err ? res.send(err).status(500) : console.log("No error");
    const request = new sql.Request(transaction)
    request.input('name', sql.NVarChar(200), req.body.Patient_strFirstName);
    request.input('lastName', sql.NVarChar(200), req.body.Patient_strLastName);
    request.input('bdate', sql.NVarChar(200), req.body.Patient_strBirthDate);
    request.input('sex', sql.NVarChar(200), req.body.Patient_strSex);
    request.input('phone', sql.NVarChar(200), req.body.Patient_strPhone);
    request.input('email', sql.NVarChar(200), req.body.Patient_strEmail);
    request.input('ss', sql.NVarChar(200), req.body.Patient_strSocialSecurity);
    request.input('addr', sql.NVarChar(200), req.body.Patient_strAddr);
    request.input('addr1', sql.NVarChar(200), req.body.Patient_strAddr1);
    request.input('zip', sql.NVarChar(200), req.body.Patient_strZip);
    request.input('state', sql.NVarChar(200), req.body.Patient_strState);
    request.input('country', sql.NVarChar(200), req.body.Patient_strCountry);
    request.input('diag', sql.NVarChar(200), req.body.Patient_strDiagnosis);
    request.input('site', sql.NVarChar(4), decodedToken.site);
    request.query("INSERT INTO tblPatient (Patient_strFirstName,Patient_strLastName,Patient_strBirthDate,Patient_strSex,Patient_strPhone,Patient_strEmail,Patient_strSocialSecurity,Patient_strAddr,Patient_strAddr1,Patient_strZip,Patient_strState,Patient_strCountry,Patient_strDiagnosis,Site_strCode) VALUES ( @name, @lastName, @bdate, @sex, @phone, @email, @ss, @addr, @addr1, @zip, @state, @country, @diag, @site )", (err, result) => {
      // ... error checks
      err ? res.send(err).status(500) : res.send(result.rowsAffected).status(200);;
      transaction.commit(err => {
        // ... error checks
        err ? console.log(err) : console.log("Commit correcto");
      })
    })
  })

});

router.get('/getPatients',verifyToken, async (req, res) => {
  const pool = await poolPromise
  const request = await pool.request();
  request.input('site', sql.NVarChar(4), decodedToken.site);
  request.query("SELECT id,Patient_strFirstName,Patient_strLastName,Patient_strSocialSecurity,Patient_strPhone,Patient_strSex FROM tblPatient WHERE Site_strCode = @site", (err,result) =>{
    if (err) {
      res.send(err)
    } else {
      if (result.recordset === undefined || result.recordset.length == 0)
      res.send('No hay pacientes registrados').status(200);
      else {
        res.json(result.recordset);
      }
    }
  });
});

router.get('/getPatient/:id', verifyToken,async (req, res) => {
  console.log(req.params.id)
  const pool = await poolPromise
  const request = await pool.request();
  request.input('id', sql.Int, req.params.id);
  request.input('site', sql.NVarChar(4), decodedToken.site);
  request.query("SELECT * FROM tblPatient WHERE id = @id AND Site_strCode = @site", (err,result) =>{
    if (err) {
      res.send(err)
    } else {
      if (result.recordset === undefined || result.recordset.length == 0)
      res.send('No se encuentra el paciente ' + req.params.id).status(200);
      else {
        res.json(result.recordset);
      }
    }
  });
});

/* END ADMINISTRATIVOS*/
module.exports = router;
