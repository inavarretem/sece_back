const sql = require('mssql')
const config = {
  user: 'sa',
  password: '2FthEUTe',
  server: 'localhost',
  database: 'SECE',
  pool: {
      max: 200,
      min: 0,
      idleTimeoutMillis: 30000
  },
  options: {
      encrypt: false // Use this if you're on Windows Azure
      ,instanceName: 'DB'
    }
}

const poolPromise = new sql.ConnectionPool(config)
  .connect()
  .then(pool => {
    console.log('Connected to SQL!')
    return pool
  })
  .catch(err => console.log('Database Connection Failed! Bad Config: ', err))

module.exports = {
  sql, poolPromise
}
